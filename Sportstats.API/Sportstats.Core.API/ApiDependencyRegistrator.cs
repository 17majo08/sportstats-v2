﻿using Sportstats.Core.API.CommandHandlers;
using Sportstats.Core.API.Commands;

namespace Sportstats.Core.API
{
	public class ApiDependencyRegistrator : IDependencyRegistrator
	{
		public void Registrations(DependencyResolver resolver)
		{
			DependencyResolver.Registrate<IApiCommandHandler<CreateSport>, CreateSportCommandHandler>();
		}
	}
}
