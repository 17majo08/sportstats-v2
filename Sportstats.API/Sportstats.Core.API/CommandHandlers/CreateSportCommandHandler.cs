﻿using Sportstats.Core.API.Commands;
using Sportstats.Core.API.Responses;
using Sportstats.Core.Model.Sports;

namespace Sportstats.Core.API.CommandHandlers
{
	public class CreateSportCommandHandler : ApiCommandHandlerBase<CreateSport>
	{
		private readonly ISportFactory sportFactory;

		public CreateSportCommandHandler(ISportFactory sportFactory)
		{
			this.sportFactory = sportFactory;
		}

		protected override void FetchData(CreateSport command)
		{
		}

		protected override void Validate(CreateSport command)
		{
		}

		protected override EntityCommandResponse HandleCommand(CreateSport command, IUnitOfWork unitOfWork)
		{
			var sport = this.sportFactory.CreateInstance(command.Name);

			unitOfWork.RegisterNew(sport);

			return new EntityCommandResponse()
			{
				Id = sport.Id,
			};
		}
	}
}
