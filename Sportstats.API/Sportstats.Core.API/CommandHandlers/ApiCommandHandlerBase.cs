﻿using Sportstats.Core.API.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.API.CommandHandlers
{
	public abstract class ApiCommandHandlerBase<TCommand> : IApiCommandHandler<TCommand>
		where TCommand : IApiCommand
	{
		public EntityCommandResponse Handle(TCommand command, IUnitOfWork unitOfWork)
		{
			this.FetchData(command);
			this.Validate(command);
			
			return this.HandleCommand(command, unitOfWork);
		}

		protected abstract void FetchData(TCommand command);
		protected abstract void Validate(TCommand command);
		protected abstract EntityCommandResponse HandleCommand(TCommand command, IUnitOfWork unitOfWork);
	}
}
