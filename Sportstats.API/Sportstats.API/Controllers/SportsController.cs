﻿using Microsoft.AspNetCore.Mvc;
using Sportstats.Core;
using Sportstats.Core.API;
using Sportstats.Core.API.Commands;
using Sportstats.Core.API.Responses;
using Sportstats.Core.Model.Sports;
using Sportstats.Persistence.NHibernate;
using Sportstats.Persistence.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sportstats.API.Controllers
{
	[ApiController]
	[Route("[controller]")]
	[Produces("application/json")]
	public class SportsController : SportstatsControllerBase
	{
		private readonly IUnitOfWork unitOfWork;
		private readonly SportRepository sportRepository;

		public SportsController()
		{
			this.unitOfWork = new UnitOfWork(this.Session);
			this.sportRepository = new SportRepository(this.Session);
		}

		/// <summary>
		/// Gets all sports
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IEnumerable<ISport> GetSports()
		{
			var sports = this.sportRepository.GetAll();

			return sports;
		}

		/// <summary>
		/// Get sport by id
		/// </summary>
		/// <returns></returns>
		[HttpGet("{id}")]
		public ISport GetSport(long id)
		{
			var sport = this.sportRepository.Get(id);

			return sport;
		}

		/// <summary>
		/// Creates a sport
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public EntityCommandResponse CreateSport(CreateSport command)
		{
			var commandHandler = this.Resolve<IApiCommandHandler<CreateSport>>();

			var response = commandHandler.Handle(command, this.unitOfWork);

			this.unitOfWork.CommitTransaction();

			return response;
		}
	}
}
