﻿using Microsoft.AspNetCore.Mvc;
using NHibernate;
using Sportstats.Core;
using Sportstats.Persistence.NHibernate;

namespace Sportstats.API.Controllers
{
	public class SportstatsControllerBase : ControllerBase
	{
		protected ISession Session { get; init; }

		public SportstatsControllerBase()
		{
			this.Session = NHibernateSessionManager.OpenSession();
		}

		protected TInterface Resolve<TInterface>()
		{
			return DependencyResolver.Resolve<TInterface>();
		}
	}
}
