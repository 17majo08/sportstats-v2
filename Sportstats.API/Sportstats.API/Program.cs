using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Sportstats.Core;

namespace Sportstats.API
{
	public class Program
	{
		public static void Main(string[] args)
		{
			//new DependencyResolver();

			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args)
		{
			return Host
				.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder => {
					webBuilder.UseStartup<Startup>();
				});
		}
	}
}
