﻿using System;

namespace Sportstats.Core.Model
{
	internal static class IdNumberGenerator
	{
		internal static long Next()
		{
			var min = 1;
			var max = long.MaxValue;

			var random = new Random();

			var uRange = (ulong)(max - min);

			ulong ulongRand;

			do
			{
				var buf = new byte[8];
				random.NextBytes(buf);
				ulongRand = (ulong)BitConverter.ToInt64(buf, 0);
			} while (ulongRand > ulong.MaxValue - ((ulong.MaxValue % uRange) + 1) % uRange);

			return (long)(ulongRand % uRange) + min;
		}
	}
}
