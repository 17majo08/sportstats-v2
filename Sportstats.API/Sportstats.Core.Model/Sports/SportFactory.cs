﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.Model.Sports
{
	public class SportFactory : ISportFactory
	{
		public ISport CreateInstance(string name)
		{
			return new Sport
			{
				Name = name
			};
		}
	}
}
