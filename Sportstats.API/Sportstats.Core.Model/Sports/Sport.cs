﻿using System.Text.Json.Serialization;

namespace Sportstats.Core.Model.Sports
{
	public class Sport : ISport
	{
		public virtual long Id { get; set; }
		public virtual string Name { get; set; }

		public Sport()
		{
			this.Id = IdNumberGenerator.Next();
		}

		[JsonIgnore]
		public virtual IAggregateRoot AggregateRoot => this;
	}
}
