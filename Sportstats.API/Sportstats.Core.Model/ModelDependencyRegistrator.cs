﻿using Sportstats.Core.Model.Sports;

namespace Sportstats.Core.Model
{
	public class ModelDependencyRegistrator : IDependencyRegistrator
	{
		public void Registrations(DependencyResolver resolver)
		{
			DependencyResolver.Registrate<ISportFactory, SportFactory>();
		}
	}
}
