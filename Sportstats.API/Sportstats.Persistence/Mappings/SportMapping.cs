﻿using NHibernate.Mapping.ByCode.Conformist;
using Sportstats.Core.Model.Sports;

namespace Sportstats.Persistence.Mappings
{
	public class SportMapping : ClassMapping<Sport>, IEntityMapping
	{
		public SportMapping()
		{
			this.Id(x => x.Id);
			this.Property(x => x.Name);
		}
	}
}
