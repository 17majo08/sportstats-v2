﻿using NHibernate;
using Sportstats.Core;
using Sportstats.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Persistence.NHibernate
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly ISession session;

		private readonly ISet<IAggregateRoot> newEntities;
		private readonly ISet<IAggregateRoot> dirtyEntities;
		private readonly ISet<IAggregateRoot> deletedEntities;

		public UnitOfWork(ISession session)
		{
			this.session = session;

			this.newEntities = new HashSet<IAggregateRoot>();
			this.dirtyEntities = new HashSet<IAggregateRoot>();
			this.deletedEntities = new HashSet<IAggregateRoot>();
		}

		public void RegisterDirty(IAggregateRoot entity)
		{
			this.dirtyEntities.Add(entity);
		}

		public void RegisterNew(IAggregateRoot entity)
		{
			this.newEntities.Add(entity);
		}

		public void RegisterRemove(IAggregateRoot entity)
		{
			this.deletedEntities.Add(entity);
		}

		public void CommitTransaction()
		{
			using var transaction = this.session.BeginTransaction();

			foreach (var entity in this.dirtyEntities)
			{
				this.session.Update(entity);
			}

			foreach (var entity in this.newEntities)
			{
				this.session.Save(entity);
			}

			foreach (var entity in this.deletedEntities)
			{
				this.session.Delete(entity);
			}

			transaction.Commit();
		}
	}
}
