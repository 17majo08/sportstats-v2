﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using Sportstats.Core.Model.Sports;
using Sportstats.Persistence.Mappings;
using System.Data;

namespace Sportstats.Persistence.NHibernate
{
	public class NHibernateSessionManager
	{
		static private readonly ISessionFactory sessionFactory;

		static NHibernateSessionManager()
		{
			var mapping = GetMappings();

			var configuration = ConfigureNHibernate();
			configuration.AddDeserializedMapping(mapping, "NHSchemaTest");

			var dialect = new MsSql2008Dialect();
			SchemaMetadataUpdater.QuoteTableAndColumns(configuration, dialect);

			sessionFactory = configuration.BuildSessionFactory();
		}

		static private Configuration ConfigureNHibernate()
		{
			var configuration = new Configuration();
			configuration.SessionFactoryName("BuildIt");

			configuration.DataBaseIntegration(db => {
				db.Dialect<MsSql2008Dialect>();
				db.Driver<SqlClientDriver>();
				db.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
				db.IsolationLevel = IsolationLevel.ReadCommitted;
				db.ConnectionString = "data source=DESKTOP-6KHBAGQ\\SQLEXPRESS;initial catalog=Sportstats;trusted_connection=true";
				db.Timeout = 10;

				// For testing
				db.LogFormattedSql = true;
				db.LogSqlInConsole = true;
				db.AutoCommentSql = true;
			});

			return configuration;
		}

		static private HbmMapping GetMappings()
		{
			var mapper = new ModelMapper();

			mapper.AddMapping<SportMapping>();

			var mappedTypes = new[]
			{
				typeof(Sport),
			};

			var mapping = mapper.CompileMappingFor(mappedTypes);

			return mapping;
		}

		public static ISession OpenSession()
		{
			return sessionFactory.OpenSession();
		}
	}
}
