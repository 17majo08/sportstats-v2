﻿using NHibernate;
using Sportstats.Core.Model.Sports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Persistence.Repositories
{
	public class SportRepository : RepositoryBase<Sport, ISport>
	{
		public SportRepository(ISession session)
			: base(session)
		{
		}
	}
}
