﻿using NHibernate;
using Sportstats.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace Sportstats.Persistence.Repositories
{
	public abstract class RepositoryBase<TEntity, TInterface>
		where TEntity : TInterface, IAggregateRoot
		where TInterface : IHasIdentity
	{
		protected ISession Session { get; init; }

		public RepositoryBase(ISession session)
		{
			this.Session = session;
		}

		public TInterface Get(long id)
		{
			var entity = this.Session.Query<TEntity>()
				.FirstOrDefault(x => x.Id == id);

			return entity;
		}

		public IEnumerable<TInterface> GetAll()
		{
			var entities = this.Session.Query<TEntity>()
				.Cast<TInterface>()
				.ToList();

			return entities;
		}

		public IDictionary<long, TInterface> GetMany(IEnumerable<long> ids)
		{
			var entityLookup = this.Session.Query<TEntity>()
				.Where(x => ids.Contains(x.Id))
				.Cast<TInterface>()
				.ToDictionary(x => x.Id);

			return entityLookup;
		}
	}
}
