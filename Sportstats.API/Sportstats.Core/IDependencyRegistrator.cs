﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core
{
	public interface IDependencyRegistrator
	{
		void Registrations(DependencyResolver resolver);
	}
}
