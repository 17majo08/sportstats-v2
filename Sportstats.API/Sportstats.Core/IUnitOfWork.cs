﻿using Sportstats.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core
{
	public interface IUnitOfWork
	{
		void RegisterNew(IAggregateRoot entity);
		void RegisterDirty(IAggregateRoot entity);
		void RegisterRemove(IAggregateRoot entity);
		void CommitTransaction();
	}
}
