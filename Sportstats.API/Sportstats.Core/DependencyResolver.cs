﻿using Sportstats.Core.API;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Sportstats.Core
{
	public sealed class DependencyResolver
	{
		private static readonly IDictionary<Type, Type> resolver = new Dictionary<Type, Type>();

		private static readonly string[] assemblies = new[]
		{
			"Sportstats.Core",
			"Sportstats.Core.API",
			"Sportstats.Core.Model",
			"Sportstats.Persistence",
		};

		static DependencyResolver()
		{
			LoadAssemblies();

			var registratorType = typeof(IDependencyRegistrator);

			var assemblies = AppDomain.CurrentDomain.GetAssemblies();

			var types = assemblies.SelectMany(x => x.GetTypes());

			var registrators2 = assemblies
				.SelectMany(a => a.GetTypes())
				.Where(t => registratorType.IsAssignableFrom(t) && !t.IsInterface);

			var registrators = registrators2
				.Select(t => Activator.CreateInstance(t))
				.Cast<IDependencyRegistrator>();
			
			var reg = new DependencyResolver();

			foreach (var registrator in registrators)
			{
				registrator.Registrations(reg);
			}
		}

		private static void LoadAssemblies()
		{
			var basePath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "..", "..", "..", "..");

			foreach (var assembly in assemblies)
			{
				var fullPath = Path.Combine(basePath, assembly, "bin", "debug", "net5.0", $"{assembly}.dll");

				Assembly.LoadFrom(fullPath);
			}
		}

		public static void Registrate<TInterface, TImplementation>()
			where TImplementation : class, TInterface
			where TInterface : class
		{
			resolver.Add(typeof(TInterface), typeof(TImplementation));
		}

		public static TInterface Resolve<TInterface>()
		{
			return (TInterface)Resolve(typeof(TInterface));
		}

		private static object Resolve(Type type)
		{
			var implementation = resolver[type];

			var constructor = implementation.GetConstructors().First();
			var parameters = constructor.GetParameters();

			var constructorArguments = new object[parameters.Length];

			foreach (var parameter in parameters)
			{
				constructorArguments[parameter.Position] = Resolve(parameter.ParameterType);
			}

			return Activator.CreateInstance(type: implementation, args: constructorArguments);
		}
	}
}
