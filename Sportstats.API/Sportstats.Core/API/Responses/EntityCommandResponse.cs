﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.API.Responses
{
	public class EntityCommandResponse
	{
		public long Id { get; set; }
	}
}
