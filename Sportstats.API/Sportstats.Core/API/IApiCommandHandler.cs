﻿using Sportstats.Core.API.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.API
{
	public interface IApiCommandHandler<TCommand>
		where TCommand : IApiCommand
	{
		EntityCommandResponse Handle(TCommand command, IUnitOfWork unitOfWork);
	}
}
