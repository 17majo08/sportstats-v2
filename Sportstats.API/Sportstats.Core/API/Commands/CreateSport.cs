﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.API.Commands
{
	public class CreateSport : IApiCommand
	{
		public string Name { get; set; }
	}
}
