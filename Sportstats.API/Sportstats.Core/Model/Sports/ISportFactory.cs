﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.Model.Sports
{
	public interface ISportFactory
	{
		ISport CreateInstance(string name);
	}
}
