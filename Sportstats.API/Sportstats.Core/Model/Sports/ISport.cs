﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.Model.Sports
{
	public interface ISport : IHasIdentity, IAggregateRoot
	{
		new long Id { get; set; }
		string Name { get; set; }
	}
}
