﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportstats.Core.Model
{
	public interface IHasIdentity
	{
		public long Id { get; set; }
	}
}
